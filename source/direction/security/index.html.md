---
layout: markdown_page
title: "Personas Direction - Security"
---

- TOC
{:toc}

## Who are security personas?
TBD

## What's next & why
TBD

### Walkthrough

This is an example of an operations flow we're trying to achieve with incident
management. Note: these are mockups of how we see the future, but the actual
features might look very different or may not ship at all.

![Sec flow](securityflow.gif)

## Key Themes
- [Auto remediation](https://gitlab.com/groups/gitlab-org/-/epics/759)
- [Static application security testing (SAST)](https://gitlab.com/groups/gitlab-org/-/epics/527)
- [Secret detection](https://gitlab.com/groups/gitlab-org/-/epics/675)
- [Bill of materials (dependency list)](https://gitlab.com/groups/gitlab-org/-/epics/858)
- [Runtime application self-protection (RASP)](https://gitlab.com/groups/gitlab-org/-/epics/443)
- [Security dashboard](https://gitlab.com/groups/gitlab-org/-/epics/275)

## Stages with security focus

- [Secure](/direction/secure)
