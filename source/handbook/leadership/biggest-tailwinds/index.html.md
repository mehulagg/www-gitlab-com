---
layout: handbook-page-toc
title: "Biggest Tailwinds"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

We are in a great market and are riding multiple tailwinds (conditions or situations that help us grow faster).

On this page, we document the biggest tailwinds and why they help us grow faster.

Biggest tailwinds have numbers attached to them for [ease of reference, not for ranking](/handbook/communication/#numbering-is-for-reference-not-as-a-signal).

## 1. [Digital Transformation](/blog/2019/03/19/reduce-cycle-time-digital-transformation/)

## 2. [Cloud native and the adoption of Kubernetes](/cloud-native/)

## 3. [Software eating the world](https://a16z.com/2011/08/20/why-software-is-eating-the-world/)

## 4. [Customer Experience](https://docs.gitlab.com/ee/ci/review_apps/index.html#visual-reviews-starter)

## 5. [DevOps](/devops)

## 6. [DevOps tooling consolidation](https://devops.com/challenges-devops-standardization/)

## 7. [Microservices](https://redmonk.com/jgovernor/2018/08/06/towards-progressive-delivery/)

## 8. [Progressive delivery](/20-years-open-source/)

## 9. [Open source](/20-years-open-source/)

## 10. [Workloads moving to the cloud](https://www.synopsys.com/blogs/software-security/cloud-migration-business/)

## 11. [All remote](/company/culture/all-remote/)

## 12. [Developers as the new kingmakers](https://dzone.com/articles/developers-are-the-new-kingmakers)

## 13. [Lack of developers](https://stackoverflow.blog/2017/03/09/developer-hiring-trends-2017/)

## 14, [Observability](https://siliconangle.com/2019/09/30/quickening-race-lead-cloud-native-computing-observability/)
